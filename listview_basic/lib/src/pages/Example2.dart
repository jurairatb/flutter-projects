import 'package:flutter/material.dart';
import 'package:listview_test/src/pages/Example3.dart';

class Example2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'ListView 2',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
            appBar: AppBar(
              title: Text("ListView 2"),
            ),
            body: ListView(
              children: [
                ListTile(
                  leading: Icon(
                    Icons.directions_railway,
                    size: 25,
                  ),
                  title: Text(
                    "8.00 A.M.",
                    style: TextStyle(fontSize: 18.0),
                  ),
                  subtitle: Text(
                    "There are many variations of passages of Lorem Ipsum available",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () {
                    //print("Train");

                  },
                ),
                Divider(
                  thickness: 1,
                ),
                ListTile(
                  leading: Icon(
                    Icons.directions_bus,
                    size: 25,
                  ),
                  title: Text(
                    "8.00 A.M.",
                    style: TextStyle(fontSize: 18.0),
                  ),
                  subtitle: Text(
                    "There are many variations of passages of Lorem Ipsum available",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () {
                    print("Bus");
                  },
                  //selected: true,
                ),
                Divider(
                  thickness: 1,
                ),
                ListTile(
                  leading: Icon(
                    Icons.directions_car,
                    size: 25,
                  ),
                  title: Text(
                    "8.00 A.M.",
                    style: TextStyle(fontSize: 18.0),
                  ),
                  subtitle: Text(
                    "There are many variations of passages of Lorem Ipsum available",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () {

                    print("Car");
                    }
                    ,
                ),
                Divider(
                  thickness: 1,
                ),
                ListTile(
                  leading: Icon(
                    Icons.directions_railway,
                    size: 25,
                  ),
                  title: Text(
                    "8.00 A.M.",
                    style: TextStyle(fontSize: 18.0),
                  ),
                  subtitle: Text(
                    "There are many variations of passages of Lorem Ipsum available",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () { print("Train 1");},
                ),
                Divider(
                  thickness: 1,
                ),
                ListTile(
                  leading: Icon(
                    Icons.directions_bus,
                    size: 25,
                  ),
                  title: Text(
                    "8.00 A.M.",
                    style: TextStyle(fontSize: 18.0),
                  ),
                  subtitle: Text(
                    "There are many variations of passages of Lorem Ipsum available",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () {
                    print("Bus 1");
                  },
                  //selected: true,
                ),
                Divider(
                  thickness: 1,
                ),
                ListTile(
                  leading: Icon(
                    Icons.directions_car,
                    size: 25,
                  ),
                  title: Text(
                    "8.00 A.M.",
                    style: TextStyle(fontSize: 18.0),
                  ),
                  subtitle: Text(
                    "There are many variations of passages of Lorem Ipsum available",
                    style: TextStyle(fontSize: 15.0),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () { print("Car 1");},
                ),
                Divider(
                  thickness: 1,
                ),
                Center(
                  child: ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: 200, height: 50),
                    child: ElevatedButton(
                      child: Text(
                        'Next',
                        style: TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => Example3()),
                        // );
                        Navigator.pushNamed(context, "/page3");
                      },
                    ),
                  ),
                ),
              ],
            )));
  }
}

import 'package:flutter/material.dart';

class Example4 extends StatelessWidget {
  final items = List<String>.generate(1000, (i) => "Item $i");

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'ListView 4',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text("ListView 4"),
          ),
          body: ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.blueAccent,
                      child: Text(
                        "$index",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      radius: 30,
                    ),
                    title: Text('${items[index]}',
                        style: TextStyle(fontSize: 18.0)),
                    subtitle: Text(
                      "There are many variations of passages of Lorem Ipsum available",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    onTap: (){
                      BuildContext dialogContext;
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          dialogContext = context;
                          return AlertDialog(
                            title: Text('Welcome'), // To display the title it is optional
                            content: Text('Flutter Dialog'), // Message which will be pop up on the screen
                            // Action widget which will provide the user to acknowledge the choice
                            actions: [
                              ElevatedButton(		 // FlatButton widget is used to make a text to work like a button
                                //textColor: Colors.black,
                                onPressed: () {
                                  Navigator.pop(dialogContext);
                                },	 // function used to perform after pressing the button
                                child: Text('CANCEL'),
                              ),
                              ElevatedButton(
                                //textColor: Colors.black,
                                onPressed: () {
                                },
                                child: Text('ACCEPT'),
                              ),
                            ],

                          );
                        },
                      );
                      //AlertDialog alert =
                    },
                  ),

                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => {
              Navigator.of(context).pop()
            },
            child: const Icon(Icons.add),
          ),
        ));
  }
}



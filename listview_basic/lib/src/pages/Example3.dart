import 'package:flutter/material.dart';

class Example3 extends StatelessWidget {
//  final items = List<String>.generate(1000, (i) => "Item $i");
  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];

  final icons = [
    Icons.directions_bike,
    Icons.directions_boat,
    Icons.directions_bus,
    Icons.directions_car,
    Icons.directions_railway,
    Icons.directions_run,
    Icons.directions_subway,
    Icons.directions_transit,
    Icons.directions_walk
  ];

  final imgs = [
    'assets/images/cat1.jpg',
    'assets/images/cat2.jpg',
    'assets/images/cat3.jpg',
    'assets/images/cat4.jpg',
    'assets/images/cat5.jpg',
    'assets/images/cat6.jpg',
    'assets/images/cat7.jpg',
    'assets/images/cat8.jpg',
    'assets/images/cat9.jpg'
  ];

  final subtitles = ['aaa aaa aaa aaa','bbb bbb bbb','ccc ccc ccc','ddd ddd ddd','eee eee eee','fff fff fff','ggg ggg ggg','hhh hhh hhh','iii iii iii'];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'ListView 3',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text("ListView 3"),
          ),
          body: ListView.builder(
            //itemCount: items.length,
            itemCount: titles.length,
            itemBuilder: (context, index) {
              return Column(
                children: [
                  ListTile(
                    leading:
                        // Icon(
                        //   //Icons.directions_railway,
                        //   icons[index],
                        //   size: 25,
                        // ),
                        CircleAvatar(
                      radius: 35,
                      backgroundImage: AssetImage(imgs[index]),
                    ),
                    //title: Text('${items[index]}',
                    title: Text('${titles[index]}',
                        style: TextStyle(fontSize: 18.0)),
                    subtitle: Text(
                      "${subtitles[index]}",
                      style: TextStyle(fontSize: 15.0),
                    ),
                    trailing: Icon(
                      Icons.star_outline,
                      size: 25,
                    ),
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            },
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => {
              Navigator.pushNamed(context, "/page4"),
            },
            child: const Icon(Icons.add),
          ),
        ));
  }
}

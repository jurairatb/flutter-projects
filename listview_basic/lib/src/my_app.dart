import 'package:flutter/material.dart';
import 'package:listview_test/src/pages/Example1.dart';
import 'package:listview_test/src/pages/Example2.dart';
import 'package:listview_test/src/pages/Example3.dart';
import 'package:listview_test/src/pages/Example4.dart';


class MyApp extends StatelessWidget {
  final _route = <String, WidgetBuilder>{
    "/page1": (BuildContext context) => Example1(),
    "/page2": (BuildContext context) => Example2(),
    "/page3": (BuildContext context) => Example3(),
    "/page4": (BuildContext context) => Example4(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: _route,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Example1(),
    );
  }
}
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final _route = <String, WidgetBuilder>{
    "/page2": (BuildContext context) => page2(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: _route,
      title: 'Basic Navigation',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Basic Navigation'),
    );
  }
}

class ScreenArguments {
  final String reportEmail;
  final String reportName;

  ScreenArguments(this.reportEmail, this.reportName);
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController nameController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: emailController,
                //obscureText: true,
                textAlign: TextAlign.left,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'PLEASE ENTER YOUR EMAIL',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: nameController,
                //obscureText: true,
                textAlign: TextAlign.left,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'PLEASE ENTER YOUR NAME',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                var result = await Navigator.pushNamed(context, "/page2",
                    arguments: ScreenArguments(
                        emailController.text, nameController.text));
                print(result);
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(SnackBar(content: Text("$result")));
              },
              child: Text("Next Page"),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                // Retrieve the text the user has entered by using the
                // TextEditingController.
                content: Column(
                  children: [
                    Text(emailController.text),
                    Text(nameController.text),
                  ],
                ),
              );
            },
          );
        },
        tooltip: 'Next Page',
        child: Icon(Icons.arrow_forward_ios),
      ),
    );
  }
}

class page2 extends StatefulWidget {
  @override
  _page2State createState() => _page2State();
}

class _page2State extends State<page2> {
  @override
  Widget build(BuildContext context) {
    final ScreenArguments args =
    ModalRoute.of(context)!.settings.arguments as ScreenArguments;
    return MaterialApp(
      title: 'Page 2',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Page 2"),
        ),
        body: Column(
          children: [
            Text(args.reportEmail),
            Text(args.reportName),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, "Hello there!");
              },
              child: Text("Back"),
            ),
          ],
        ),
      ),
    );
  }
}

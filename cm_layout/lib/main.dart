import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const title = "Codemobiles";

    // TODO: implement build
    return MaterialApp(
        title: title, // Recent apps (android) ,  App switcher (iOS)
        home: Scaffold(
          appBar: AppBar(
            title: Text(title),
          ),
          body: ListView(
            children: <Widget>[
              Column(
                children: [
                  headerSection,
                  titleSection,
                  buttonSection,
                  courseSection
                ],
              )
            ],
          ),
        ));
  }
}

Widget headerSection = Image.network(
    "http://www.codemobiles.com/biz/images/contact_us_1.jpg");

Widget titleSection = Container(
  padding: EdgeInsets.all(50),
  child: Row(
    children: <Widget>[
      Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Text(
                  "CodeMobiles Co., Ltd",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                margin: EdgeInsets.only(bottom: 8),
              ),
              Text(
                "Bangkok, Thailand",
                style: TextStyle(color: Colors.grey[500]),
              )
            ],
          )),
      Icon(
        Icons.thumb_up,
        color: Colors.blue[500],
      ),
      Container(
        margin: EdgeInsets.only(left: 4),
        child: Text("99"),
      )
    ],
  ),
);

Widget buttonSection = Container(
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      _buildButtonColumn(icon: Icons.thumb_up, label: "Like"),
      _buildButtonColumn(icon: Icons.comment, label: "Comment"),
      _buildButtonColumn(icon: Icons.share, label: "Share")
    ],
  ),
);

Column _buildButtonColumn({required IconData icon, required String label}) {
  Color? icColor = Colors.grey[500];

  return Column(children: <Widget>[
    Icon(icon, color: icColor),
    Container(
      margin: EdgeInsets.only(top: 12),
      child: Text(label,
          style: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.bold,
            color: icColor,
          )),
    )
  ]);
}

Widget courseSection = Container(
  margin: EdgeInsets.only(top: 70),
  padding: EdgeInsets.all(8),
  child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.all(8),
        child: Text(
          "Mobile & Web Courses",
        ),
      ),
      Container(
        height: 120,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            _buildListCard(
                url:
                'http://www.codemobiles.com/biz/training/images/angular/angular_banner.jpg?dummy=856930545'),
            _buildListCard(
                url:
                'http://www.codemobiles.com/biz/training/images/asp_dot_net_core/asp_core_banner.jpg?dummy=306900965'),
            _buildListCard(
                url:
                'http://www.codemobiles.com/biz/training/images/android_banner.jpg?dummy=1542011419'),
            _buildListCard(
                url:
                'http://www.codemobiles.com/biz/training/images/ios_training_header.jpg?dummy=1550469413'),
          ],
        ),
      )
    ],
  ),
);

Card _buildListCard({required String url}) {
  return Card(
      child: FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: url,
      ));
}


class RegisterArg {
  final String reportEmail;
  final String reportPassword;

  RegisterArg(this.reportEmail, this.reportPassword);
}
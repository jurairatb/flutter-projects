
import 'package:flutter/material.dart';
import 'package:navigator_textfield/src/models/register_arg.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final RegisterArg args =
    ModalRoute.of(context)!.settings.arguments as RegisterArg;
    return Scaffold(appBar: AppBar(
      title: Text("Welcome....."),
    ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(args.reportEmail),
            Text(args.reportPassword),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, "Hello there!");
              },
              child: Text("Back"),
            ),
          ],
        ),
      ),
    );
  }
}

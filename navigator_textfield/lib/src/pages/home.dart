
import 'package:flutter/material.dart';
import 'package:navigator_textfield/src/models/register_arg.dart';

class Home extends StatefulWidget {
  const Home({Key? key, required this.title}) : super(key: key);



  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController pwdController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: emailController,
                //obscureText: true,
                textAlign: TextAlign.left,
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.email,
                    color: Colors.green,),
                  filled: true,
                  fillColor: Colors.green[100],
                  border:
                  OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  //border:InputBorder.none,
                  hintText: 'PLEASE ENTER YOUR EMAIL',
                  hintStyle: TextStyle(color: Colors.green),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextField(
                controller: pwdController,
                //obscureText: true,
                textAlign: TextAlign.left,
                obscureText: true,
                decoration: InputDecoration(
                  //border: InputBorder.none,
                  prefixIcon: Icon(
                    Icons.password,
                    color: Colors.green,),
                  filled: true,
                  fillColor: Colors.green[100],
                  border:
                  OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(
                      width: 0,
                      style: BorderStyle.none,
                    ),
                  ),
                  hintText: 'PLEASE ENTER YOUR PASSWORD',

                  hintStyle: TextStyle(color: Colors.grey),
                ),

              ),
            ),
            ElevatedButton(
              onPressed: () async {
                var result = await Navigator.pushNamed(context, "/page2",
                    arguments: RegisterArg(
                        emailController.text, pwdController.text));
                print(result);
                ScaffoldMessenger.of(context)
                  ..removeCurrentSnackBar()
                  ..showSnackBar(SnackBar(content: Text("$result")));
              },
              child: Text("Next Page"),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                // Retrieve the text the user has entered by using the
                // TextEditingController.
                content: Column(
                  children: [
                    Text(emailController.text),
                    Text(pwdController.text),
                  ],
                ),
              );
            },
          );
        },
        tooltip: 'Next Page',
        child: Icon(Icons.arrow_forward_ios),
      ),
    );
  }
}
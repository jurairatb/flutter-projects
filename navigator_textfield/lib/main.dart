import 'package:flutter/material.dart';
import 'package:navigator_textfield/src/pages/home.dart';
import 'package:navigator_textfield/src/pages/welcome_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final _route = <String, WidgetBuilder>{
    "/page2": (BuildContext context) => WelcomePage(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      routes: _route,
      home: const Home(title: 'Basic Navigator'),
    );
  }
}


